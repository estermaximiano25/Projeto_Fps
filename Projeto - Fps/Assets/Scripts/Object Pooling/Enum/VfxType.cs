﻿public enum VfxType
{
    EXPLOSION_CARTOON = 0,
    EXPLOSION_SIMPLE = 1,
    SKELETON_SPAWN = 2,
    KNIGHT_SPAWN = 3,
    WIZARD_SPAWN = 4,
    DARK_DEATH = 5,
    LIGHT_DEATH = 6,
    FIRE = 7
}
